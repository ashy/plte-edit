#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <stdbool.h>

// Uncomment if running on a big endian computer
// #define USE_BIG_ENDIAN

#ifndef USE_BIG_ENDIAN
	uint32_t reverseBytes(uint32_t x) {
		return ((x >> 24) & 0x000000ff) |
			((x >> 8)  & 0x0000ff00) |
			((x << 8)  & 0x00ff0000) |
			((x << 24) & 0xff000000);
	}
#endif

#define printUsage(cmd) printf("Usage: %1$s <file> [palette index 0] [index 1] [index 2]...\n <file> must be an indexed PNG image, or otherwise support and contain a PLTE chunk.\n\n Modifies the color palette of <file> according to the colors specified, or if no colors are specified, it will instead print the current palette.\n\nExamples:\n %1$s hi.png 99139c\n  Changes index 0 of hi.png to #99139c\n\n %1$s meep.png\n  Print the current color palette of meep.png\n ", cmd);	

const uint8_t SIGNATURE[16] = {
	0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, // Actual signature
	0x00, 0x00, 0x00, 0x0D, // IHDR length
	0x49, 0x48, 0x44, 0x52  // IHDR
}; // byte pattern at start of every png

const uint8_t PLTE[4] = {0x50, 0x4C, 0x54, 0x45}; // byte pattern indicating PLTE chunk

// CRC code is adapted from the example in the png spec

uint32_t crcTable[256]; // Table of CRCs of all 8-bit messages
bool crcTableComputed = false; // Has the table been computed? Initially false

// Make the table for a fast CRC
void makeCrcTable() {
	uint32_t c;

	for(uint_fast16_t n = 0; n < 256; n++) {
		c = (uint32_t)n;
		for(uint_fast8_t k = 0; k < 8; k++) {
			if(c & 1) {
				c = 0xedb88320 ^ (c >> 1);
			} else {
				c = c >> 1;
			}
		}
		crcTable[n] = c;
	}
	crcTableComputed = true;
}

// Update a running CRC with the bytes buf[0..len-1]--the CRC should be initialized to all 1's, and the transmitted value
// is the 1's complement of the final running CRC (see the
// crc() routine below))
uint32_t updateCrc(uint32_t crc, const uint8_t *buf, uint32_t len) {
	uint32_t c = crc;

	if(!crcTableComputed) {
		makeCrcTable();
	}
	for(uint32_t n = 0; n < len; n++) {
		c = crcTable[(c ^ buf[n]) & 0xff] ^ (c >> 8);
	}
	return c;
}

// Return the CRC of the bytes buf[0..len-1]
uint32_t crc(const uint8_t *buf, uint32_t len) {
	return updateCrc(0xffffffff, buf, len) ^ 0xffffffff;
}

// Returns the size of the PLTE chunk in bytes, or 0 if invalid
// Note: this is not a comprehensive check, this is just to verify the file's readability for the purposes of this program.
uint32_t verifyPNG(FILE *file) {
	uint8_t sig[16];
	fread(sig, 16, 1, file);
	if(memcmp(SIGNATURE, sig, 16)) {
		return 0;
	}

	uint8_t colorType;
	fseek(file, 25, SEEK_SET);
	fread(&colorType, 1, 1, file);
	if(!(colorType == 3 || colorType == 2 || colorType == 6)) {
		return 0;
	}

	fseek(file, 33, SEEK_SET); // Start at first chunk after IHDR
	while(true) {
		uint32_t length;
		if(!fread(&length, 4, 1, file)) {
			return 0;
		}
#ifndef USE_BIG_ENDIAN
		length = reverseBytes(length);
#endif

		uint8_t type[4];
		if(!fread(type, 4, 1, file)) {
			return 0;
		}
		
		if(!memcmp(PLTE, type, 4)) {
			if(length % 3) {
				return 0;
			}
			return length;
		}

		if(fseek(file, length + 4, SEEK_CUR)) {
			return 0;
		}
	}
}

int main(int argc, char **argv) {
	if(argc < 2) {
		printUsage(argv[0]);
		return EXIT_FAILURE;
	}

	FILE *file = fopen(argv[1], argc < 3 ? "rb" : "r+b");
	if(file == NULL) {
		perror("Couldn't open file");
		return EXIT_FAILURE;
	}

	uint32_t plteSize = verifyPNG(file);
	if(!plteSize) {
		printf("I don't like this file.\n");
		return EXIT_FAILURE;
	}

	if(argc < 3) { // Print palette
		uint8_t palette[plteSize];
		fread(palette, plteSize, 1, file);

		printf("Color palette of %s:\n", argv[1]);
		for(uint32_t i = 0; i < plteSize/3; i++) {
#ifndef USE_BIG_ENDIAN
			printf(" %02" PRIu32 ": #%06" PRIx32 "\n", i, reverseBytes(*((uint32_t *)&(palette[i*3]))) >> 8);
#else
			printf(" %02" PRIu32 ": #%06" PRIx32 "\n", i, *((uint32_t *)&(palette[i*3])) >> 8);
#endif
		}
		fclose(file);
		return EXIT_SUCCESS;
	}

	// Edit palette
	int newPlteSize = (argc-2)*3;
	if(newPlteSize > plteSize) {
		printf("Too many arguments (max %" PRIu32 " for %s)\n", plteSize/3 + 1, argv[1]);
		return EXIT_FAILURE;
	}

	uint8_t newPlte[newPlteSize];
	for(int i = 2; i < argc; i++) {
		if(strlen(argv[i]) != 6) {
			printf("You typed the color thingy wrong\n");
			return EXIT_FAILURE;
		}
		unsigned long x = strtoul(argv[i], NULL, 16);
		
		newPlte[(i-2)*3] = x >> 16 & 0xff;
		newPlte[(i-2)*3+1] = x >> 8 & 0xff;
		newPlte[(i-2)*3+2] = x & 0xff;
	}
	fseek(file, 0, SEEK_CUR);
	fwrite(newPlte, newPlteSize, 1, file);

	// Calculate new CRC
	uint32_t c = updateCrc(0xffffffff, PLTE, 4);
	c = updateCrc(c, newPlte, newPlteSize);
	
	if(plteSize-newPlteSize > 0) {
		uint8_t oldPlte[plteSize-newPlteSize];
		fread(oldPlte, plteSize-newPlteSize, 1, file);
		fseek(file, 0, SEEK_CUR);
		c = updateCrc(c, oldPlte, plteSize-newPlteSize);
	} else {
		fseek(file, plteSize-newPlteSize, SEEK_CUR);
	}
	c ^= 0xffffffff;
#ifndef USE_BIG_ENDIAN
	c = reverseBytes(c);
#endif

	fwrite(&c, 4, 1, file);

	fclose(file);
	return EXIT_SUCCESS;
}