A command line tool to edit the PLTE chunk of PNGs

To build, run `gcc plte_edit.c -o plte_edit`

### Usage

`plte_edit <file> [palette index 0] [index 1] [index 2]...`

`<file>` is the file you want to change the palette of, and must be an indexed PNG image, or otherwise support and contain a PLTE chunk.\
If no colors are specified, it will instead print the current palette.

#### Examples:
`plte_edit hi.png 99139c`\
Changes index 0 of `hi.png` to #99139c

`plte_edit meep.png`\
Print the current color palette of `meep.png`